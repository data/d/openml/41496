# OpenML dataset: DRSongsLyrics

https://www.openml.org/d/41496

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains 358 lyrics of songs for the rock bands 'The Rolling Stones' and 'Deep Purple'. The bands are equally represented in the dataset (179 songs for each band). This dataset was extracted from the much larger 'SongLyrics' dataset created by Sergey Kuznetsov.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41496) of an [OpenML dataset](https://www.openml.org/d/41496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

